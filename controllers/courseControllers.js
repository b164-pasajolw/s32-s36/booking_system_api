const Course = require("../models/Course") ;

//Create a new course
/*
Steps:
	1. Create a new Course object
	2. Save to the database
	3. error handling
*/

//s34 discussion

/*
module.exports.addCourse = (reqBody) => {
	console.log(reqBody);

	//Create a new object
	let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	});

	//Saves the created object to our database
	return newCourse.save().then((course, error) =>{
		//Course creation failed
		if(error) {
			return false;
		} else {
			//Course Creation successful
			return true;
		}
	})
}

*/

// s34 Activity 

//from Maam Judy


module.exports.addCourse = (reqBody) => {

	console.log(reqBody);

	let newCourse = new Course({
		name: reqBody.course.name,
		description: reqBody.course.description,
		price: reqBody.course.price
	})

	return newCourse.save().then((course, error) => {
		if (error) {
			return false;
		} else{
			return true;
		}
	})
}



/* from Sir Orlando

module.exports.addCourse = (reqBody) => {

	console.log(reqBody);

	let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	})

	return newCourse.save().then((course, error) => {
		if (error) {
			return false;
		} else{
			return true;
		}
	})
}


*/


//Retrieving All Courses

module.exports.getAllCourses = () => {
	return Course.find({}).then( result => {
		return result;
	})
}


//Retrieve all Active courses

module.exports.getAllActive = () => {
	return Course.find({ isActive: true }).then(result => {
		return result;
	})
}

//retrieving a specific course

module.exports.getAllCourse = (reqParams) => {
	return Course.findById(reqParams).then(result => {
		return result;
	})
}

//Update a course

/*

//Update a course
/*
Steps:
1. Create a variable which will contain the information retrieved from the request body
2. Find and update course using the course ID

*/

module.exports.updateCourse = (courseId, reqBody) => {
	//specify the properties of the doc to be uupdated

	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};

	//findByIdandUpdate(id, updatesTobeApplied)
	return Course.findByIdAndUpdate(courseId, updatedCourse).then((course, error) => {
		//course not updated
		if(error) {
			return false;
		} else {
			//course updated successfully
			return true;
		}
	})
}


//s35 activity (archiving a course)

module.exports.notActive = (courseId, reqBody) => {
	
	let updatedCourse = {
		isActive : reqBody.isActive
	};

	return Course.findByIdAndUpdate(courseId, updatedCourse).then((course, error) => {

		if(error) {
			return false;
		} else {
			return true;
		}
	})
}
