const express = require ("express");
const router = express.Router();
const CourseController = require("../controllers/courseControllers");

const auth = require('../auth');



/*
//Creating a course
router.post("/", (req, res) => {
	CourseController.addCourse(req.body).then(result => res.send(result));
})
*/

//s34 Activity

//from maam Judy


router.post("/", auth.verify, (req, res) => {

	const data =  {
		course:req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin) {
	CourseController.addCourse(data).then(result => res.send(result));
	} else {
		res.send({ auth: "You're not an admin"})
	}
})





/* from Sir Orlando

router.post("/", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	isAdmin = userData.isAdmin;

	console.log(isAdmin);

	if (isAdmin) {

	courseController.addCourse(req.body).then(result => res.send(result));
}	else{
	
	res.send(false);
}


});


*/


//Retrieving ALL courses

router.get("/all", (req, res) => {
	CourseController.getAllCourses().then(result => res.send(result));
})

//Retrieving all ACTIVE courses
router.get("/", (req, res) => {
	CourseController.getAllActive().then(result => res.send(result))
});


//retrieving a specific course
router.get("/:courseId", (req,res) => {
	console.log(req.params.courseId);

	CourseController.getAllCourse(req.params.courseId).then(result => res.send(result))
})


//Update a course

router.put("/:courseId", auth.verify, (req, res) => {
const data = {
	isAdmin: auth.decode(req.headers.authorization).isAdmin
}
	
	if(data.isAdmin){
		CourseController.updateCourse(req.params.courseId, req.body).then(result => res.send(result));
	} else {
		res.send(false)
	}
})


//s35 activity (archiving a course)

router.put("/:courseId/archive", auth.verify, (req, res) => {
const data = {
	isAdmin: auth.decode(req.headers.authorization).isAdmin
}
	
	if(data.isAdmin){
		CourseController.notActive(req.params.courseId, req.body).then(result => res.send(result));
	} else {
		res.send(false)
	}
})





//Enroll user to a course

router.post("/enroll", auth.verify, (req, rees) => {
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		courseId : req.body.courseId
	}


	  
})



module.exports = router;